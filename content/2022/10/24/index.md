---
title: "2. Fediverse Moderationstreff"
author: ""
date: 2022-10-24
lastmod: 2022-10-24
categories: [Protokoll]
tags: [Protokoll, Fediverse, FediverseModerationsTreff, FMT]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "Hier ist das Protokoll des 2. FediverseModerationTreff vom 24. Oktober 2022 zu finden."
---

Hier ist das Protokoll des 2. FediverseModerationTreff vom 24. Oktober 2022 zu finden.

<!--more-->