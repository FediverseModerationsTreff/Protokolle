---
title: "1. Fediverse Moderationstreff"
author: ""
date: 2022-09-26
lastmod: 2022-09-26
categories: [Protokoll]
tags: [Protokoll, Fediverse, FediverseModerationsTreff, FMT]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "Hier ist das Protokoll des 1. FediverseModerationTreff vom 26. September 2022 zu finden."
---

Hier ist das Protokoll des 1. FediverseModerationTreff vom 26. September 2022 zu finden.

<!--more-->