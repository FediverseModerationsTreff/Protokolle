---
title: "3. Fediverse Moderationstreff"
author: ""
date: 2022-11-28
lastmod: 2022-11-28
categories: [Protokoll]
tags: [Protokoll, Fediverse, FediverseModerationsTreff, FMT]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "Hier ist das Protokoll des 3. FediverseModerationTreff vom 28. November 2022 zu finden."
---

Hier ist das Protokoll des 3. FediverseModerationTreff vom 28. November 2022 zu finden.

<!--more-->