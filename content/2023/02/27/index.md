---
title: "4. Fediverse Moderationstreff"
author: ""
date: 2023-02-27
lastmod: 2023-02-27
categories: [Protokoll]
tags: [Protokoll, Fediverse, FediverseModerationsTreff, FMT]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "Hier ist das Protokoll des 4. FediverseModerationTreff vom 27. Februar 2023 zu finden."
---

Hier ist das Protokoll des 4. FediverseModerationTreff vom 27. Februar 2023 zu finden.

<!--more-->