---
title: "7. Fediverse Moderationstreff"
author: ""
date: 2023-07-20
lastmod: 2023-07-20
categories: [Protokoll]
tags: [Protokoll, Fediverse, FediverseModerationsTreff, FMT]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "Hier ist das Protokoll des 7. FediverseModerationTreff vom 20. Juli 2023 zu finden."
---

Hier ist das Protokoll des 7. FediverseModerationTreff vom 20. Juli 2023 zu finden.

<!--more-->