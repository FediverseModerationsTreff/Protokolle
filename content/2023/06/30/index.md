---
title: "6. Fediverse Moderationstreff"
author: ""
date: 2023-06-30
lastmod: 2023-06-30
categories: [Protokoll]
tags: [Protokoll, Fediverse, FediverseModerationsTreff, FMT]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "Hier ist das Protokoll des 6. FediverseModerationTreff vom 30. Juni 2023 zu finden."
---

Hier ist das Protokoll des 6. FediverseModerationTreff vom 30. Juni 2023 zu finden.

<!--more-->