---
title: "5. Fediverse Moderationstreff"
author: ""
date: 2023-05-15
lastmod: 2023-05-15
categories: [Protokoll]
tags: [Protokoll, Fediverse, FediverseModerationsTreff, FMT]
draft: false
enableDisqus: false
enableMathJax: false
toc: true
disableToC: false
disableAutoCollapse: true
description: "Hier ist das Protokoll des 5. FediverseModerationTreff vom 28. November 2022 zu finden."
---

Hier ist das Protokoll des 5. FediverseModerationTreff vom 28. November 2022 zu finden.

<!--more-->